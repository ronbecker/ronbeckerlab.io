# ronbecker.gitlab.io

A personal gitlab pages site for me, Ron Becker. Made using `buster` and a local install of [Ghost](https://ghost.org/).

Right now it's literally just the base install, and prepop articles, etc. I'll replace all that later with my own stuff. This is just to get it started.

